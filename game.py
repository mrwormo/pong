#!/usr/bin/env python3

# Copyright (c) 2017, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

import pygame, sys

class Game():

    clay = (0xFF, 0x40, 0)
    weed = (0x02, 0x91, 0x1E)

    def __init__(self, bgd):
        # Screen setup

        self.width = 800
        self.height = 600
        self.bgd = bgd

        self.ball_speed = [ -2, -2 ]
        self.racket1_speed = [ 0, 0 ]
        self.racket2_speed = [ 0, 0 ]

        # Pygame initialization
        pygame.init()
        self.screen = pygame.display.set_mode( (self.width, self.height) )

        # Load resources
        self.ball = pygame.image.load("image/ball.png")
        self.ball_coords = self.ball.get_rect()

        self.racket1 = pygame.image.load("image/racket.png")
        self.racket1_coords = self.racket1.get_rect()

        self.racket2 = pygame.image.load("image/racket.png")
        self.racket2_coords = self.racket2.get_rect()
        self.racket2_coords.right = self.width

        self.throw()

    # Throw ball from center
    def throw(self):
        self.ball_coords.left = self.width/2
        self.ball_coords.top = self.height/2

    def draw(self):
        # Display everything
        self.screen.fill(self.bgd)
        self.screen.blit(self.ball, self.ball_coords)
        self.screen.blit(self.racket1, self.racket1_coords)
        self.screen.blit(self.racket2, self.racket2_coords)
        pygame.display.flip()

        # sleep 10ms, since there is no need for more than 100Hz refresh :)
        pygame.time.delay(10)

   # throw()

    def run_server(self, sock):

        while True:
            for e in pygame.event.get():
                # Check for exit
                if e.type == pygame.QUIT:
                    sys.exit()

                # Check for racket movements
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_UP:
                        self.racket1_speed[1] = -4
                        pass
                    elif e.key == pygame.K_DOWN:
                        self.racket1_speed[1] = 4
                        pass

                elif e.type == pygame.KEYUP:
                    if e.key == pygame.K_UP:
                        self.racket1_speed[1] = 0
                        pass
                    elif e.key == pygame.K_DOWN:
                        self.racket1_speed[1] = 0
                        pass

                #else:
                #    print(e)

            # Move ball
            self.ball_coords = self.ball_coords.move(self.ball_speed)
            # Bounce ball on walls
            if self.ball_coords.left < 0 or self.ball_coords.right >= self.width:
               self.ball_speed[0] = -self.ball_speed[0]
            if self.ball_coords.top < 0 or self.ball_coords.bottom >= self.height:
               self.ball_speed[1] = -self.ball_speed[1]

            # Move racket 1
            self.racket1_coords = self.racket1_coords.move(self.racket1_speed)
            # Clip racket on court
            if self.racket1_coords.left < 0:
                self.racket1_coords.left = 0
            elif self.racket1_coords.right >= self.width:
                self.racket1_coords.right = self.width-1
            if self.racket1_coords.top < 0:
                self.racket1_coords.top = 0
            elif self.racket1_coords.bottom >= self.height:
                self.racket1_coords.bottom = self.height-1

            # Move racket 2
            self.racket2_coords = self.racket2_coords.move(self.racket2_speed)
            # Clip racket on court
            if self.racket2_coords.left < 0:
                self.racket2_coords.left = 0
            elif self.racket2_coords.right >= self.width:
                self.racket2_coords.right = self.width-1
            if self.racket2_coords.top < 0:
                self.racket2_coords.top = 0
            elif self.racket2_coords.bottom >= self.height:
                self.racket2_coords.bottom = self.height-1

            # Racket reached racket position?
            if self.ball_coords.left <= 0:
                if self.ball_coords.bottom <= self.racket1_coords.top or self.ball_coords.top >= self.racket1_coords.bottom:
                    print("lost!")
                    sock.send(b"WINNIE")
                    self.throw()
            if self.ball_coords.right >= self.width:
                if self.ball_coords.bottom <= self.racket2_coords.top or self.ball_coords.top >= self.racket2_coords.bottom:
                    print("win!")
                    sock.send(b"LOUSEUR")
                    self.throw()
            coordinates = ""
            coordinates += str(self.ball_coords.topleft) + ":"
            coordinates += str(self.racket1_coords.topleft) + ":"
            coordinates += str(self.racket2_coords.topleft) + ":"
            sock.send(coordinates.encode('utf-8'))
            self.draw()

    def run_client(self, sock):
        while True:
            for e in pygame.event.get():
                # Check for exit
                if e.type == pygame.QUIT:
                    sys.exit()

                # Check for racket movements
                elif e.type == pygame.KEYDOWN:
                    if e.key == pygame.K_UP:
                        sock.send(b"K-UP")
                    elif e.key == pygame.K_DOWN:
                        sock.send(b"K-DOWN")

                elif e.type == pygame.KEYUP:
                    if e.key == pygame.K_UP:
                        sock.send(b"UP_RLS")
                    elif e.key == pygame.K_DOWN:
                        sock.send(b"DOWN_RLS")
                
            self.draw()
