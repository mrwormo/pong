#!/usr/bin/env python3

# Copyright (c) 2017, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

import sys, socket
from game import Game
from _thread import *
import threading 


class Client():
    """classe de connexion client"""
    def __init__(self, address):
        self.address = address 
        self.game = Game(Game.clay)

    def connect(self, s): 
        """Connect to server"""   
        try:
            s.connect((self.address, 7777))
            print("Client connected!")
        except socket.gaierror:
            print("Hote inexistant")
            sys.exit (1)
        except Exception as e:
            print(e, type(e))
            print("Connexion refusée.")
            sys.exit (1)
        start_new_thread(self.thread_reception, (s,))
        self.game.run_client(s)

    def thread_reception(self, sock):
        """Data reception and treatment"""
        while True:
            data = sock.recv(1024)
            # print("Server sent : " + str(data))
            if data == b"WINNIE":
                print("win!")
            elif data == b"LOUSEUR":
                print("lost!")
            else:
                self.game.ball_coords = eval(data.decode('utf-8').split(":")[0])
                self.game.racket1_coords = eval(data.decode('utf-8').split(":")[1])
                self.game.racket2_coords = eval(data.decode('utf-8').split(":")[2])