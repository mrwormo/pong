#!/usr/bin/env python3

# Copyright (c) 2017, 2020 Samuel Thibault <samuel.thibault@ens-lyon.org>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY Samuel Thibault ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

from game import Game
from _thread import *
import threading 


class Server():
    """classe de connexion server"""
    # Initialisation :
    def __init__(self, s):
        self.l = [s]

    def listen(self, s):
        """Launching listening server"""
        s.bind(("", 7777))
        s.listen()
        print("Server listen")
        sock, a = s.accept()
        print("Nouveau client :", a)
        self.l.append(sock)
        self.game = Game(Game.weed)
        start_new_thread(self.thread_reception, (sock,)) 
        self.game.run_server(sock)
    
    # Data reception and treatement :
    def thread_reception(self, sock):
        """Data reception and treatment"""
        while True:
            data = sock.recv(1024)
            #print("Server sent : " + str(data))
            if data == b"K-UP":
                self.game.racket2_speed[1] = -4
            elif data == b"K-DOWN":
                self.game.racket2_speed[1] = 4
            elif data == b"UP_RLS":
                self.game.racket2_speed[1] = 0
            elif data == b"DOWN_RLS":
                self.game.racket2_speed[1] = 0
